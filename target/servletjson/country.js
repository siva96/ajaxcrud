var mode = "";
function loadData() {
    // alert('loadData!');
    $.ajax({
        url: "http://localhost:9090/country",
        type: 'GET',

        headers: {
            "Accept": "application/json",
            "Content-Type": "application/json"
        },
        success: function (data) {
            // alert(JSON.stringify(data));
            //alert(data.length);
            $("#countries").append("<table class='table' ><tr> <th width='50'>Id</th>"
                + "<th width='50'>name</th> </tr></table>");
            var tr;
            
            for (var i = 0; i < data.length; i++) {
                tr = $('<tr/>');
                tr.append("<td >" + data[i].id + "</td>");
                tr.append("<td>" + data[i].name + "</td>");
                var d = JSON.stringify(data[i]);
                //alert(d);
                tr.append("<td><button onclick='edit(" + d + ")' >Edit</button> <button onclick=remove(" + d + ") >Delete</button></td>");
                $('table').append(tr);
            }
        },
        error: function () {
            alert('fail....post');

        }
    });
}

$(document).ready(function () {

    // Some code to be executed...
    alert("Hello World!");
    loadData();

    $("input:submit").click(function () {
        saveorupdate();


    })
});

function saveorupdate() {
    if (mode != "") {
        alert("update clicked...")
        update();
    }
    else {
        alert("save clicked....")
        save();
    }
}

function save() {
    alert("post");
    var newCountry = {

        "id": $('#id').val(),
        "name": $('#name').val()
    };
    console.log(newCountry);
    $.ajax({
        type: "POST",
        url: "http://localhost:9090/country",
        data: JSON.stringify(newCountry),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) { 
            alert(data); 
            console.log(data);
         },
        failure: function (errMsg) {
            console.log(errMsg);
            alert(errMsg);
            loadData();
        }
    });
}

function edit(data) {
    alert("update....." + JSON.stringify(data));
    // updateID = data.id;
    mode = "edit";

    if (mode == "edit") { console.log(mode); $("#btn").text("update"); }

    $("#id").val(data.id);
    $("#name").val(data.name);
    loadData();
}

function update() {
    alert("calling update")
    var updateCountry = {

        "id": $('#id').val(),
        "name": $('#name').val()
    };
    console.log(updateCountry);
    console.log("id is..." + $('#id').val());
    alert("id is..." + $('#id').val());
    $.ajax({
        type: "PUT",
        url: "http://localhost:9090/country",
        data: JSON.stringify(updateCountry),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) { alert(data); console.log(data); },
        failure: function (errMsg) {
            console.log(errMsg);
            alert(errMsg);
        }
    });
}

function remove(data) {
    alert("delete....." + JSON.stringify(data));
    var removeCountry = {

        "id": $('#id').val(),
        "name": $('#name').val()
    };
    console.log(removeCountry);
    $.ajax({
        type: "DELETE",
        url: "http://localhost:9090/country",
        data: JSON.stringify(data),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (result) {

            $("#countries").html(result);
            loadData();
        }
    });
}